package id.co.telkomsel.sa;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.blueprint.sd.AuthenticatedUser;
import org.blueprint.sd.JwtEncoderUtil;
import org.blueprint.sd.JwtResourceOwnerDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtEncoderUtilTests {
	
	public static String getJsonPayload() {
		String token = "f54cadb9ab854c68dd19e7d260daff59838710619a0e7287f864dfb811594d9e";
		String scopes = "public user orders";
		String refreshToken = "7f44207acbe1aae8fbe30567611a436e38b6667e15949204f483a55f3b4a20bc";
		long expiresIn = 1500L;
		String resourceOwnerId = "12806809";
		String applicationId = "23";
		String applicationName = "MyApps";
		
		JwtResourceOwnerDto resourceOwner = new JwtResourceOwnerDto();
		resourceOwner.setId("12806809");
		resourceOwner.setLastLoginAt("2018-04-09T08:32:36Z");
		resourceOwner.setSpammer(Boolean.FALSE);
		resourceOwner.setRole("admin");
		
		AuthenticatedUser user = new AuthenticatedUser(
				token, scopes, refreshToken, expiresIn, 
				resourceOwnerId, resourceOwner, 
				applicationId, applicationName);
		
		ObjectMapper mapper = new ObjectMapper();
		String retval="";
		try {
			retval = mapper.writeValueAsString(user);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return retval;
	}

	@Test
	public void testEncode() {
		String payload = getJsonPayload();
		String secretKey ="ondeondebulet";
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		System.out.println("Creating JWT");
		String has = JwtEncoderUtil.createJWT(signatureAlgorithm, secretKey, payload);
		String expected="eyJhbGciOiJIUzI1NiJ9.eyJ0b2tlbiI6ImY1NGNhZGI5YWI4NTRjNjhkZDE5ZTdkMjYwZGFmZjU5ODM4NzEwNjE5YTBlNzI4N2Y4NjRkZmI4MTE1OTRkOWUiLCJzY29wZXMiOiJwdWJsaWMgdXNlciBvcmRlcnMiLCJyZWZyZXNoX3Rva2VuIjoiN2Y0NDIwN2FjYmUxYWFlOGZiZTMwNTY3NjExYTQzNmUzOGI2NjY3ZTE1OTQ5MjA0ZjQ4M2E1NWYzYjRhMjBiYyIsImV4cGlyZXNfaW4iOjE1MDAsInJlc291cmNlX293bmVyX2lkIjoiMTI4MDY4MDkiLCJyZXNvdXJjZV9vd25lciI6eyJpZCI6IjEyODA2ODA5Iiwicm9sZSI6ImFkbWluIiwibGFzdF9sb2dpbl9hdCI6IjIwMTgtMDQtMDlUMDg6MzI6MzZaIiwic3BhbW1lciI6ZmFsc2V9LCJhcHBsaWNhdGlvbl9pZCI6IjIzIiwiYXBwbGljYXRpb25fbmFtZSI6Ik15QXBwcyJ9.8sbwMFKvlvzvNJYM2Nw3Y4qHvf5RRcVC5LYvbupk-Js";
		System.out.println("JWT: ["+has+"]");
		assertEquals(expected, has);
	}
}
