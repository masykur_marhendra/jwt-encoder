package org.blueprint.sd;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class AuthenticatedUser {

	@JsonProperty("token")
	private String token;
	@JsonProperty("scopes")
	private String scopes;
	@JsonProperty("refresh_token")
	private Object refreshToken;
	@JsonProperty("expires_in")
	private Long expiresIn;
	@JsonProperty("resource_owner_id")
	private String resourceOwnerId;

	@JsonProperty("resource_owner")
	private JwtResourceOwnerDto resourceOwner;
	@JsonProperty("application_id")
	private String applicationId;
	@JsonProperty("application_name")
	private String applicationName;
    
    public String getToken() {
        return token;
    }
    
    @Override
    public String toString() {
    	ObjectMapper mapper = new ObjectMapper();
    	try {
			String str = mapper.writeValueAsString(this);
			return str;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
    	return super.toString();
    }


}
