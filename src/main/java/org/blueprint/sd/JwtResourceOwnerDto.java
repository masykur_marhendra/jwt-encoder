package org.blueprint.sd;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"id",
	"role",
	"last_login_at",
	"spammer",
})
public class JwtResourceOwnerDto {
	
	@JsonProperty("id")
	private String id;
	@JsonProperty("role")
	private String role;
	@JsonProperty("last_login_at")
	private String lastLoginAt;
	@JsonProperty("spammer")
	private Boolean spammer;
}
