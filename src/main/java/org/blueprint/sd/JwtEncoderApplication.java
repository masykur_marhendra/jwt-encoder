package org.blueprint.sd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtEncoderApplication {
	public static void main(String[] args) {
		SpringApplication.run(JwtEncoderApplication.class, args);
	}

}
