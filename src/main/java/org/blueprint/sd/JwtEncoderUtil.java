package org.blueprint.sd;

import java.security.Key;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtEncoderUtil {

	public static String createJWT(SignatureAlgorithm signatureAlgorithm, String secretKey, String payload) {

	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

	    JwtBuilder builder = Jwts.builder()
	            .setPayload(payload)
	            .signWith(signatureAlgorithm, signingKey);
	  
	    return builder.compact();
	}
}
